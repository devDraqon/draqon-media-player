# Draqons Media Player
This module is a reimplementation of the PodLove Web-Player (in React and Storybook), which originally has been created by Tim Pritlove (<a href="https://podlove.org/podlove-web-player/">click here to visit the original Podlove Player</a>)

## Live Demo
<a href="https://storybook.draqondevelops.com/react-media-player"> click here </a> to visit a static live version of this module.

## Installation
    npm install --save draqon-media-player

### Importing a function
    import MediaPlayer from 'draqon-media-player';

### Source Code
<a href="https://gitlab.com/devdraqon/draqon-media-player"> visit the source code for this project here. </a>

### Known Bugs
-   no documentated known bugs

### Report Bugs and Issues
If you encounter any bugs, issues, or have improvements which you'd like to share, please choose one of the following ways to report your issue.
- <a href="https://gitlab.com/devDraqon/draqon-media-player/-/issues"> Report Issue on Gitlab </a> (requires having a gitlab account).
- <a href="mailto:draqondevelops@gmail.com"> Mail your Issue </a> to me.

### Version History
-   Start of documentated version history 