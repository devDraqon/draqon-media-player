import React, {useState} from "react";
import { BodyProps } from "./Body.types";
import Controls from "../Controls/Controls";
import { useMediaQuery } from 'react-responsive'
import { Link, Flex, Paragraph, Icon, Headline } from "draqon-component-library";
import "./Body.scss";
import Audio from "../Audio/Audio";
import Chapters from "../Chapters/Chapters";
import Downloads from "../Downloads/Downloads";
import Share from "../Share/Share";
import Info from "../Info/Info";
import Transcript from "../Transcript/Transcript";
import Menu from "../Menu/Menu";

const Body: React.FC<BodyProps> = ({ onVolumeChange, onSpeedChange, episode, podcast, settings }: BodyProps) => {
  const isMobileScreen = useMediaQuery({ query: '(max-width: 850px)' })
  const [selectedMenuItem, setSelectedMenuItem] = useState(0)
  const updateSelectedItem = (id) => { setSelectedMenuItem(id === selectedMenuItem ? -1 : id) }
  const [volume, setVolume] = useState(100);
  const updateVolume = (volume) => { 
    setVolume(volume); 
    onVolumeChange ? onVolumeChange(volume) : null;
  }
  const [speed, setSpeed] = useState(50);
  const updateSpeed = (speed) => { 
    setSpeed(speed); 
    onSpeedChange ? onSpeedChange(speed) : null;
  }

  return(
    <div className="Body">
        <Menu onSelectionChange={updateSelectedItem} settings={JSON.parse(JSON.stringify({
          hasChapters: settings.hasChapters,
          hasTranscript: settings.hasTranscript,
          volume: volume
        }))} />
        {selectedMenuItem === 0 ? <Info episode={episode} podcast={podcast} /> : null}
        {selectedMenuItem === 1 ? <Chapters chapters={episode.chapters} /> : null}
        {selectedMenuItem === 2 ? <Transcript /> : null}
        {selectedMenuItem === 3 ? <Share episode={episode} podcast={podcast} /> : null}
        {selectedMenuItem === 4 ? <Downloads links={episode.downloads} /> : null}
        {selectedMenuItem === 5 ?
          <Flex classList={["player__ranges"]} direction="vertical">
            <Audio volume={volume} speed={speed} onSpeedChange={(speed) => updateSpeed(speed)} onVolumeChange={(volume) => updateVolume(volume)} />
          </Flex> : null}
    </div>
)};

export default Body;