export interface BodyProps {
  episode: any;
  settings: {
    hasTranscript: boolean;
    hasChapters: boolean;
    volume: number;
  }
  onSelectionChange?: any;
  onVolumeChange?: any;
  onSpeedChange?: any;
  podcast: any;
  changeChapterCallback?: any;
}