import React from "react";
import Menu from './Menu';

export default {
  title: "Body Atoms|Menu"
};

const menuSettingsComplete = {
  hasChapters: true, 
  hasTranscript: true, 
  volume: 26 
}
const menuSettingsWithoutChapters = {
  hasChapters: false, 
  hasTranscript: true, 
  volume: 51 
}
const menuSettingsWithoutTranscript = {
  hasChapters: true, 
  hasTranscript: false, 
  volume: 76 
}
const menuSetingsWithoutChaptersAndWithoutTranscript = {
  hasChapters: false, 
  hasTranscript: false, 
  volume: 12 
}


export const Complete = () => <Menu settings={menuSettingsComplete} />;
export const WithoutChapters = () => <Menu settings={menuSettingsWithoutChapters} />;
export const WithoutTranscript = () => <Menu settings={menuSettingsWithoutTranscript} />;
export const WithoutChaptersAndWithoutTranscript = () => <Menu settings={menuSetingsWithoutChaptersAndWithoutTranscript} />;