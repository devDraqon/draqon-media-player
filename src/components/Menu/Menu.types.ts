export interface MenuProps {
    settings: {
        hasTranscript: boolean;
        hasChapters: boolean;
        volume: number;
    }
    onSelectionChange?: any;
}