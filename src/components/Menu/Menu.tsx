import React, { useState } from "react";
import { Range, Flex, Icon, Paragraph, Headline } from "draqon-component-library";
import { useMediaQuery } from 'react-responsive'
import { MenuProps } from "./Menu.types";
import "./Menu.scss";


const Menu = ({settings, onSelectionChange}: MenuProps) => {
  const [selectedMenuItem, setSelectedMenuItem] = useState(0)
  const updateSelectedItem = (id) => { 
    setSelectedMenuItem(id === selectedMenuItem ? -1 : id) 
    onSelectionChange ? onSelectionChange(id === selectedMenuItem ? -1 : id) : null;
  }
  const isMobileScreen = useMediaQuery({ query: '(max-width: 850px)' })

  return (
    <Flex classList={["player__menu"]} direction="horizontal">
    <div onClick={() => updateSelectedItem(0)} className={selectedMenuItem !== 0 ? "player__menu-item-box" : "player__menu-item-box player__menu-item-box--highlighted"}>
      <Flex classList={["player__menu-item"]} direction="horizontal">
        <Icon src="https://files.draqondevelops.com/images/info.png" callbackOnClick={null} alt="Info" />
        {!isMobileScreen ? <Paragraph size="small"> Info </Paragraph> : null}
      </Flex>
    </div>
    {settings.hasChapters === true ?
      <div onClick={() => updateSelectedItem(1)} className={selectedMenuItem !== 1 ? "player__menu-item-box" : "player__menu-item-box player__menu-item-box--highlighted"}>
        <Flex classList={["player__menu-item"]} direction="horizontal">
          <Icon src="https://files.draqondevelops.com/images/chapters.png" callbackOnClick={null} alt="Chapters" />
          {!isMobileScreen ? <Paragraph size="small"> Chapter </Paragraph> : null}
        </Flex>
      </div> : null}
    {settings.hasTranscript === true ?
      <div onClick={() => updateSelectedItem(2)} className={selectedMenuItem !== 2 ? "player__menu-item-box" : "player__menu-item-box player__menu-item-box--highlighted"}>
        <Flex classList={["player__menu-item"]} direction="horizontal">
          <Icon src="https://files.draqondevelops.com/images/transcript.png" callbackOnClick={null} alt="Transcript" />
          {!isMobileScreen ? <Paragraph size="small"> Transcript </Paragraph> : null}
        </Flex>
      </div> : null}
    <div onClick={() => updateSelectedItem(3)} className={selectedMenuItem !== 3 ? "player__menu-item-box" : "player__menu-item-box player__menu-item-box--highlighted"}>
      <Flex classList={["player__menu-item"]} direction="horizontal">
        <Icon src="https://files.draqondevelops.com/images/share.png" callbackOnClick={null} alt="Share" />
        {!isMobileScreen ? <Paragraph size="small"> Share </Paragraph> : null}
      </Flex>
    </div>
    <div onClick={() => updateSelectedItem(4)} className={selectedMenuItem !== 4 ? "player__menu-item-box" : "player__menu-item-box player__menu-item-box--highlighted"}>
      <Flex classList={["player__menu-item"]} direction="horizontal">
        <Icon src="https://files.draqondevelops.com/images/cloud.png" callbackOnClick={null} alt="cloud" />
        {!isMobileScreen ? <Paragraph size="small"> Download </Paragraph> : null}
      </Flex>
    </div>
    <div onClick={() => updateSelectedItem(5)} className={selectedMenuItem !== 5 ? "player__menu-item-box" : "player__menu-item-box player__menu-item-box--highlighted"}>
      <Flex classList={["player__menu-item"]} direction="horizontal">
        {settings.volume < 25 ? <Icon classList={["volume__loudest"]} src="https://files.draqondevelops.com/images/speaker_0.png" alt="icon" callbackOnClick={null} />
          : settings.volume < 50 ? <Icon classList={["volume__loud"]} src="https://files.draqondevelops.com/images/speaker_1.png" alt="icon" callbackOnClick={null} />
            : settings.volume < 75 ? <Icon classList={["volume__normal"]} src="https://files.draqondevelops.com/images/speaker_2.png" alt="icon" callbackOnClick={null} />
              : settings.volume >= 75 ? <Icon classList={["volume__quiet"]} src="https://files.draqondevelops.com/images/speaker_3.png" alt="icon" callbackOnClick={null} /> : null}
        {!isMobileScreen ? <Paragraph size="small"> Audio </Paragraph> : null}
      </Flex>
    </div>
  </Flex>
  )
}

export default Menu;