import React from "react";
import Header from './Header';

export default {
  title: "Structure|Header"
};

const episode = {
  title: "LNP 194 - Nicht mal die Ausnahmen sind erlaubt",
  released: "7.6.2012, 11:14",
  duration: "41 Minutes",
  description: "Die Gesellschaft rüstet sich für das neue Internetprotokoll und die Bundeswehr für kommende Kriegsszenarien im digitalen Kontext. Die Netzaktivisten wiederum rüsten sich für den ACTA-Aktionstag am Wochenende während die Politik weiterhin gelangweilt das Thema Vorratsdatenspeicherung hin und her schiebt. Wir berichten.",
  link: "https://logbuch-netzpolitik.de/lnp027",
  downloads: [
    {
      label: "MPEG-4 AAC Audio (m4a)",
      filesize: "31 MB audio/mp4",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.m4a"
    },
    {
      label: "Opus Audio (opus)",
      filesize: "30 MB audio/opus",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.opus"
    },
    {
      label: "Ogg Vorbis Audio (oga)",
      filesize: "32 MB audio/oga",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.oga"
    },
    {
      label: "MP3 Audio (mp3)",
      filesize: "50 MB audio/mpeg",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.mp3"
    }
  ],
  chapters: [
    {
      title: "Prolog",
      endtime: "06:21"
    },
    {
      title: "Feedback",
      endtime: "12:49"
    },
    {
      title: "Religion Abschaffen?",
      endtime: "19:56"
    },
    {
      title: "Kapitalismus als Waffe",
      endtime: "28:11"
    },
    {
      title: "Politiker auf Abwegen",
      endtime: "35:42"
    },
    {
      title: "Die Grenze des Lobbyismus",
      endtime: "41:23"
    },
    {
      title: "Epilog",
      endtime: "46:26"
    },
    {
      title: "Bonustrack",
      endtime: "51:17"
    }
  ]
}

export const Default = () => <Header time={0} episode={episode} />;
