import React, {useState, useEffect} from "react";
import { HeaderProps } from "./Header.types";
import Controls from "../Controls/Controls";
import { Link, Flex, Paragraph, Icon, Headline } from "draqon-component-library";
import Title from "../Title/Title";
import "./Header.scss";
import Timeline from "../Timeline/Timeline";

const Header: React.FC<HeaderProps> = ({rewind, onTimelineChange, timelinePos, forward, time, duration, onPausedChange, episode }: HeaderProps) => {
  
  return (
    <div className="header">
        <Title episode={episode} />
        <Controls rewind15Sec={rewind} forward30Sec={forward} onPausedChange={(paused) => onPausedChange(paused)} />
        <Timeline timelinePos={timelinePos} onTimelineChange={onTimelineChange} time={time} duration={duration} />
    </div>
)};

export default Header;