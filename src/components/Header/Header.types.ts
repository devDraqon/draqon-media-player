export interface HeaderProps {
  episode: any;
  onPausedChange?: any;
  rewind?: any; 
  forward?: any; 
  time?: any;
  timelinePos?: any;
  duration?: any; 
  onTimelineChange?: any;
}