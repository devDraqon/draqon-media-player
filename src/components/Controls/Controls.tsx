import React, {useState} from "react";
import { ControlsProps } from "./Controls.types";
import {Icon, Range, Headline, Flex, Paragraph } from "draqon-component-library";
import "./Controls.scss";

const Controls: React.FC<ControlsProps> = ({ rewind15Sec, forward30Sec, onPausedChange }) => {
  const [isPaused, setPaued] = useState(true);
  const togglePaused = () => {
    setPaued(!isPaused);
    onPausedChange ? onPausedChange(!isPaused) : null;
  }


  return(
  <div className="controls" >
    <Flex classList={["controls__flex"]} direction="horizontal">
      <Icon alt="arrow.png" callbackOnClick={null} classList={["icon__rotated--left"]} src="https://files.draqondevelops.com/images/arrow.png" />
      <Icon alt="rewind.png" callbackOnClick={rewind15Sec} src="https://files.draqondevelops.com/images/rewind.png" />
      {isPaused ? <Icon alt="play.png" callbackOnClick={togglePaused} src="https://files.draqondevelops.com/images/play.png" /> 
      : <Icon alt="pause.png" callbackOnClick={togglePaused} src="https://files.draqondevelops.com/images/pause.png" /> }
      <Icon alt="forward.png" callbackOnClick={forward30Sec} src="https://files.draqondevelops.com/images/forward.png" />
      <Icon alt="arrow.png" callbackOnClick={null} classList={["icon__rotated--right"]} src="https://files.draqondevelops.com/images/arrow.png" />
    </Flex>
  </div>
)};

export default Controls;