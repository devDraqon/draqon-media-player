import React, {useState} from "react";
import { useMediaQuery } from 'react-responsive'
import { InfoProps } from "./Info.types";
import {Icon, Link, Headline, Flex, Paragraph, Image } from "draqon-component-library";
import "./Info.scss";

const Info: React.FC<InfoProps> = ({ episode, podcast }) => {
  const isMobileScreen = useMediaQuery({ query: '(max-width: 640px)' })
  const isMiniScreen = useMediaQuery({ query: '(max-width: 460px)' })

  return(
  <div className="info" >
    <Flex classList={["info__sections"]} direction={isMobileScreen ? "vertical" : "horizontal"}>
      <div className="info__episode info__section">
        <Headline size="small"> {episode.title} </Headline>
        <Flex classList={["info__flex"]} direction="horizontal">
          <Icon classList={["info__icon"]} alt="calendar.png" callbackOnClick={null} src="https://files.draqondevelops.com/images/calendar.png" />
          <Paragraph size="small"> {episode.released} </Paragraph>
        </Flex>
        <Flex classList={["info__flex"]} direction="horizontal">
          <Icon classList={["info__icon"]} alt="clock.png" callbackOnClick={null} src="https://files.draqondevelops.com/images/clock.png" />
          <Paragraph size="small"> {episode.duration} </Paragraph>
        </Flex>
        <Paragraph size="small"> {episode.description} </Paragraph>
        <Flex classList={["info__flex", "info__flex-link"]} direction="horizontal">
          <Icon src="https://files.draqondevelops.com/images/link.png" alt="link.png" callbackOnClick={null} />
          <Link isReactLink={false} target="_blank" url={episode.link}> <Paragraph size="small"> {episode.link} </Paragraph> </Link>
        </Flex>
      </div>
      <div className="info__podcast info__section">
          <Flex classList={isMobileScreen ? ["flex__mobile"] : []} direction={isMiniScreen ? "vertical" : isMobileScreen ? "horizontal" : "vertical"}>
          <div className={isMiniScreen ? null : isMobileScreen ? "flex__mobile-item" : null}>
            <Headline size="xsmall"> {podcast.title} </Headline>
            <Image alt="logo.png" src={podcast.image} />
          </div>
          <div className={isMiniScreen ? null : isMobileScreen ? "flex__mobile-item" : null}>
            <Paragraph size="small"> {podcast.description} </Paragraph>
            <Flex classList={isMobileScreen ? ["flex__mobile-item", "info__flex", "info__flex-link"] : ["info__flex", "info__flex-link"]} direction="horizontal">
              <Icon src="https://files.draqondevelops.com/images/link.png" alt="link.png" callbackOnClick={null} />
              <Link isReactLink={false} target="_blank" url={podcast.link}> <Paragraph size="small"> {podcast.link} </Paragraph> </Link>
            </Flex>
          </div>
          </Flex>
      </div>
    </Flex>
  </div>
)};

export default Info;