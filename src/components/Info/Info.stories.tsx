import React from "react";
import Info from './Info';

export default {
  title: "Body Atoms|Info"
};

const episode = {
  title: "LNP 194 - Nicht mal die Ausnahmen sind erlaubt",
  released: "7.6.2012, 11:14",
  duration: "41 Minutes",
  description: "Die Gesellschaft rüstet sich für das neue Internetprotokoll und die Bundeswehr für kommende Kriegsszenarien im digitalen Kontext. Die Netzaktivisten wiederum rüsten sich für den ACTA-Aktionstag am Wochenende während die Politik weiterhin gelangweilt das Thema Vorratsdatenspeicherung hin und her schiebt. Wir berichten.",
  link: "https://logbuch-netzpolitik.de/lnp027"
}

const podcast = {
  title: "Logbuch:Netzpolitik",
  link: "https://logbuch-netzpolitik.de",
  description: "Logbuch:Netzpolitik ist ein in der Regel wöchentlich erscheinender Podcast, der im Dialog zwischen Linus Neumann und Tim Pritlove die wichtigsten Themen und Ereignisse mit netzpolitischem Bezug aufgreift und diskutiert.",
  image: "https://logbuch-netzpolitik.de/wp-content/cache/podlove/b9/9b910c8f72bf73f840020b263af2ef/logbuchnetzpolitik_500x.jpg",
}

export const Default = () => <Info episode={episode} podcast={podcast}/>;