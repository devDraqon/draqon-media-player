export interface InfoProps {
    episode: {
        title: string
        released: string
        duration: string
        description: string
        link: string
    },
    podcast: {
        title: string
        link: string
        description: string
        image: string
    }
}