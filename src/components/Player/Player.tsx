import React, { useEffect, useRef, useState } from "react";
import { PlayerProps, AudioElemProps } from "./Player.types";
import Body from "../Body/Body";
import Header from "../Header/Header";
import "./Player.scss";


const episode = {
  title: "LNP 194 - Nicht mal die Ausnahmen sind erlaubt",
  released: "7.6.2012, 11:14",
  duration: "41 Minutes",
  description: "Die Gesellschaft rüstet sich für das neue Internetprotokoll und die Bundeswehr für kommende Kriegsszenarien im digitalen Kontext. Die Netzaktivisten wiederum rüsten sich für den ACTA-Aktionstag am Wochenende während die Politik weiterhin gelangweilt das Thema Vorratsdatenspeicherung hin und her schiebt. Wir berichten.",
  link: "https://logbuch-netzpolitik.de/lnp027",
  downloads: [
    {
      label: "MPEG-4 AAC Audio (m4a)",
      filesize: "31 MB audio/mp4",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.m4a"
    },
    {
      label: "Opus Audio (opus)",
      filesize: "30 MB audio/opus",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.opus"
    },
    {
      label: "Ogg Vorbis Audio (oga)",
      filesize: "32 MB audio/oga",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.oga"
    },
    {
      label: "MP3 Audio (mp3)",
      filesize: "50 MB audio/mpeg",
      url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.mp3"
    }
  ],
  chapters: [
    {
      title: "Prolog",
      endtime: "06:21"
    },
    {
      title: "Feedback",
      endtime: "08:49"
    },
    {
      title: "Religion Abschaffen?",
      endtime: "19:56"
    },
    {
      title: "Kapitalismus als Waffe",
      endtime: "09:11"
    },
    {
      title: "Politiker auf Abwegen",
      endtime: "12:42"
    },
    {
      title: "Die Grenze des Lobbyismus",
      endtime: "11:23"
    },
    {
      title: "Epilog",
      endtime: "06:26"
    },
    {
      title: "Bonustrack",
      endtime: "01:17"
    }
  ]
}

const podcast = {
  title: "Logbuch:Netzpolitik",
  link: "https://logbuch-netzpolitik.de",
  description: "Logbuch:Netzpolitik ist ein in der Regel wöchentlich erscheinender Podcast, der im Dialog zwischen Linus Neumann und Tim Pritlove die wichtigsten Themen und Ereignisse mit netzpolitischem Bezug aufgreift und diskutiert.",
  image: "https://logbuch-netzpolitik.de/wp-content/cache/podlove/b9/9b910c8f72bf73f840020b263af2ef/logbuchnetzpolitik_500x.jpg",
}

const Player = ({ hasTranscript, hasChapters }: PlayerProps) => {

  const [audioMetaDataLoaded, setAudioMetaDataLoaded] = useState(false);
  const [timelinePos, setTimelinePos] = useState(0);


  const togglePause = (paused: boolean) => {
    const audioEl: HTMLAudioElement = AudioSrcRef.current;
    console.log(paused)
    !paused ? audioEl.play() : audioEl.pause();
  }

  const updateSpeed = (speed: number) => {
    const audioEl: HTMLAudioElement = AudioSrcRef.current;
    let newspeed = speed / 100 + 0.5
    console.log(newspeed)
    if(speed / 100 < 0.5) {
    }
    audioEl.playbackRate = newspeed;
  }

  const updateVolume = (volume: number) => {
    const audioEl: HTMLAudioElement = AudioSrcRef.current;
    audioEl.volume = (volume / 100);
    console.log(volume);
  }

  const audioTimeUpdate = () => {
    if(AudioSrcRef) {
      const audioEl: HTMLAudioElement = AudioSrcRef.current;
      setTimelinePos((audioEl.currentTime / audioEl.duration) * 100)
      setTimeCallback(audioEl.currentTime);
    }
  }

  const [duration, setDuration] = useState(0);
  const [time, setTime] = useState(0);

  const setTimeCallback = (time: number) => {
    setTime(time)
  }

  const setDurationCallback = (time: number) => {
    setDuration(time)
  }

  const rewind = () => {
    if(AudioSrcRef) {
      const audioEl: HTMLAudioElement = AudioSrcRef.current;
      audioEl.currentTime -= 15;
    }
  }

  const forward = () => {
    if(AudioSrcRef) {
      const audioEl: HTMLAudioElement = AudioSrcRef.current;
      audioEl.currentTime += 30;
    }
  }

  const onTimelineChange = (pos) => {
    if(AudioSrcRef) {
      let newpos = (duration * (pos / 100));
      const audioEl: HTMLAudioElement = AudioSrcRef.current;
      audioEl.currentTime = newpos;
      setTimeCallback ? setTimeCallback(newpos) : null;
      setTimelinePos(pos);
    }
  }

  useEffect(() => {
    
    const audioEl: HTMLAudioElement = AudioSrcRef.current;
    audioEl.onloadedmetadata = () => {
      setDurationCallback ? setDurationCallback(audioEl.duration) : null;
      audioEl.ontimeupdate = () => audioTimeUpdate();
    };
  }, [])
  
  
  const AudioSrcRef = useRef();
  
  return (
    <div className="player">
      <Header 
        onTimelineChange={onTimelineChange}
        timelinePos={timelinePos}
        rewind={rewind}
        forward={forward}
        duration={duration} 
        time={time} 
        onPausedChange={(paused) => togglePause(paused)} 
        episode={episode} 
      />

    <audio ref={AudioSrcRef} >
    <source src={"https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.m4a"} />
  </audio>
        <Body settings={JSON.parse(JSON.stringify({
          hasChapters: hasChapters, 
          hasTranscript: hasTranscript 
        }))} 
        podcast={podcast} 
        episode={episode} 
        onSpeedChange={updateSpeed}
        onVolumeChange={updateVolume}
      />

    </div>
  )
};

export default Player;