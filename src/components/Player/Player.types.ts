export interface PlayerProps {
    hasTranscript: boolean;
    hasChapters: boolean;
}
export interface AudioElemProps {
    getRef: any;
    setDurationCallback: any;
    audioTimeUpdateCallback: any;
}