import React from "react";
import Player from './Player';

export default {
  title: "Main|Player"
};

export const Complete = () => <Player hasChapters={true} hasTranscript={true} />;
export const WithoutChapters = () => <Player hasChapters={false} hasTranscript={true} />;
export const WithoutTranscript = () => <Player hasChapters={true} hasTranscript={false} />;
export const WithoutChaptersAndWithoutTranscript = () => <Player hasChapters={false} hasTranscript={false} />;