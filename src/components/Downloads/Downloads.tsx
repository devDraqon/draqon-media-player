import React from "react";
import { DownloadsProps } from "./Downloads.types";
import { Link, Flex, Paragraph, Icon } from "draqon-component-library";
import "./Downloads.scss";

const Downloads: React.FC<DownloadsProps> = ({ links }) => (
  <div className="downloads">
    <Flex classList={["downloads__list"]} direction="vertical">
      {links.map((link, id) => 
        <div  key={id} className="downloads__icon">
        <Flex classList={["downloads__link"]} direction="vertical">

          <Flex classList={["downloads__link-iconwrap"]} direction="horizontal">
            <Icon classList={["musicnote"]} callbackOnClick={null} src="https://files.draqondevelops.com/images/musicnote.png" alt="musicnote.png" />
            <Flex classList={["downloads__link-textwrap"]} direction="vertical">
              <Paragraph size="medium"> {link.label} </Paragraph>
              <Paragraph size="small"> {link.filesize} </Paragraph>
            </Flex>
          </Flex>

          <Flex classList={["downloads__buttons"]} direction="horizontal">
            <Icon classList={["downloads__button downloads__button-square"]} src="https://files.draqondevelops.com/images/link.png" alt="link.png" callbackOnClick={null} />
            <Link classList={["downloads__button"]} url={link.url} isReactLink={false} target={"_self"}> <Paragraph size="medium"> DOWNLOAD </Paragraph> </Link>
          </Flex>
        </Flex>

      </div> )}
    </Flex>
  </div>
);

export default Downloads;