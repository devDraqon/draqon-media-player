export interface DownloadsProps {
  links: Array<{
    url: string;
    label: string;
    filesize: string;
  }>
}