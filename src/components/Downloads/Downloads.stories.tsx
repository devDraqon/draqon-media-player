import React from "react";
import Downloads from './Downloads';

export default {
  title: "Body Atoms|Downloads"
};

export const Default = () => <Downloads links={[
  {
    label: "MPEG-4 AAC Audio (m4a)",
    filesize: "31 MB audio/mp4",
    url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.m4a"
  },
  {
    label: "Opus Audio (opus)",
    filesize: "30 MB audio/opus",
    url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.opus"
  },
  {
    label: "Ogg Vorbis Audio (oga)",
    filesize: "32 MB audio/oga",
    url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.oga"
  },
  {
    label: "MP3 Audio (mp3)",
    filesize: "50 MB audio/mpeg",
    url: "https://files.draqondevelops.com/audio/lnp194-nicht-mal-die-ausnahmen-sind-erlaubt.mp3"
  }
]} />;
