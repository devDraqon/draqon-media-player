import React, {useState, useEffect} from "react";
import { TimelineProps } from "./Timeline.types";
import {Icon, Range, Headline, Flex, Paragraph } from "draqon-component-library";
import "./Timeline.scss";

const Timeline: React.FC<TimelineProps> = ({ onTimelineChange, timelinePos, duration, time }) => {

  const convertTimeForDsiplay = (seconds) => {
    var date = new Date(0);
    date.setSeconds(seconds);
    return date.toISOString().substr(11, 8) 
  }
  
  return(
  <div className="timeline" >

    <div className="timeline__chapters" >
      <div className="timeline__chapter" />
      <Range pins={[ convertTimeForDsiplay(time ? time : 0), convertTimeForDsiplay(duration ? duration : 0)]} step={0.1} min={0} max={100} callbackOnChange={onTimelineChange} value={timelinePos} />
    </div>
    <Paragraph size="small"> Chapter Name </Paragraph>
  </div>
)};

export default Timeline;