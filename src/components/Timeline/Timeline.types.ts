export interface TimelineProps {
    duration: number;
    time: number;
    timelinePos: any;
    onTimelineChange?: any;
}