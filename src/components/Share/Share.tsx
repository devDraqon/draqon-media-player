import React, {useState} from "react";
import { ShareProps } from "./Share.types";
import {Icon, Range, Headline, Flex, Paragraph, DropDown } from "draqon-component-library";
import "./Share.scss";

const Share: React.FC<ShareProps> = ({ episode, podcast }) => {

  return(
  <div className="share" >
    <Headline size="small"> Share </Headline>
    <Flex direction="horizontal">
      <Flex classList={["share__item"]} direction="vertical">
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/star.png"} />
        <Headline size="small"> SHOW </Headline>
        <Paragraph size="small"> Show name </Paragraph>
      </Flex>
      <Flex classList={["share__item"]} direction="vertical">
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/cut.png"} />
        <Headline size="small"> EPISODE </Headline>
        <Paragraph size="small"> Episode name </Paragraph>
      </Flex>
      <Flex classList={["share__item"]} direction="vertical">
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/chapters.png"} />
        <Headline size="small"> CHAPTER </Headline>
        <Paragraph size="small"> Chapter name </Paragraph>
      </Flex>
      <Flex classList={["share__item"]} direction="vertical">
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/play.png"} />
        <Headline size="small"> TIME </Headline>
        <Paragraph size="small"> 00:00 </Paragraph>
      </Flex>
    </Flex>
    <div className="share__socials">
      <Paragraph size="medium"> Share In...</Paragraph>
      <Flex direction="horizontal">
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/twitter.png"} />
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/facebook.png"} />
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/pinterest.png"} />
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/reddit.png"} />
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/mail.png"} />
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/instagram.png"} />
        <Icon callbackOnClick={null} alt="star.png" src={"https://files.draqondevelops.com/images/linkedin.png"} />
      </Flex>
    </div>
    <div className="share__copy">
      <Paragraph size="medium"> Link </Paragraph>
      <div className="share__copy-flex">
        <Flex direction="horizontal">
          <Paragraph classList={["copy"]} size="small"> COPY </Paragraph>
          <input type="text" className={"iframe"} value="https://logbuch-netzpolitik.de/lnp027" />
        </Flex>
      </div>
      <Paragraph size="medium"> Embed Code </Paragraph>
      <div className="share__copy-flex">
        <Flex direction="horizontal"> 
          <Paragraph classList={["copy"]} size="small"> COPY </Paragraph>
          <DropDown  callbackOnChange={null} labels={["250x400", "320x400", "375x400", "600x290", "768x290"]} />
          <input type="text" className={"iframe"} value='<iframe title="Podlove Web Player: Logbuch:Netzpolitik - LNP027 Vogel des Jahres" width="320" height="400" src="https://cdn.podlove.org/web-player/share.html?episode=https%3A%2F%2Flogbuch-netzpolitik.de%2F%3Fpodlove_player4%3D208" frameborder="0" scrolling="no" tabindex="0"></iframe>' />
        </Flex>
      </div>
    </div>
  </div>
)};

export default Share;