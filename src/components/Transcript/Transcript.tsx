import React, {useState} from "react";
import { TranscriptProps } from "./Transcript.types";
import {Icon, Range, Headline, Flex } from "draqon-component-library";
import "./Transcript.scss";

const Transcript: React.FC<TranscriptProps> = ({ }) => {

  return(
  <div className="transcript" >
    <Headline size="small"> Transcript </Headline>
  </div>
)};

export default Transcript;