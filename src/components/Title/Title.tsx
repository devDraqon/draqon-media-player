import React from "react";
import { TitleProps } from "./Title.types";
import { Flex, Paragraph, Icon, Headline } from "draqon-component-library";
import "./Title.scss";

const Title: React.FC<TitleProps> = ({ episode }: TitleProps) => (
  <div className="title">
      <Flex classList={["podcast__flex"]} direction="horizontal">
        <Icon classList={["podcast"]} alt="podcast.png" callbackOnClick={null} src="https://logbuch-netzpolitik.de/wp-content/cache/podlove/b9/9b910c8f72bf73f840020b263af2ef/logbuchnetzpolitik_500x.jpg" />
        <Flex direction="vertical">
          <Paragraph size="small"> Logbuch:Netzpolitik </Paragraph>
          <Headline size="small"> LNP027 Vogel des Jahres </Headline>
        </Flex>
      </Flex>
  </div>
);

export default Title;