import React, {useState} from "react";
import {Icon, Range, Headline, Flex } from "draqon-component-library";
import { SpeedProps, VolumeProps, AudioProps } from "./Audio.types";
import "./Audio.scss";

const Volume: React.FC<VolumeProps> = ({ volume, pins, onVolumeChange }: VolumeProps) => {
  
  return(
  <div className="volume" >
    <Headline size="small"> Volume </Headline>
    <Flex classList={["volume__flex"]} direction="horizontal">
      <div className="volume__icon">
        {volume < 25 ?  <Icon classList={["volume__loudest"]} src="https://files.draqondevelops.com/images/speaker_0.png" alt="icon" callbackOnClick={null} />
        :volume < 50 ?  <Icon classList={["volume__loud"]} src="https://files.draqondevelops.com/images/speaker_1.png" alt="icon" callbackOnClick={null} />
        :volume < 75 ?  <Icon classList={["volume__normal"]} src="https://files.draqondevelops.com/images/speaker_2.png" alt="icon" callbackOnClick={null} />
        :volume >= 75?  <Icon classList={["volume__quiet"]} src="https://files.draqondevelops.com/images/speaker_3.png" alt="icon" callbackOnClick={null} /> : null}
      </div>
      <div className="volume__Control">
        <Range pins={pins} callbackOnChange={onVolumeChange} value={volume} min={0} max={100} step={1}/>
      </div>
    </Flex>
  </div>
)};

const Speed: React.FC<SpeedProps> = ({ speed, onSpeedChange, pins }: SpeedProps) => {

  return(
  <div className="speed" >
    <Headline size="small"> Speed </Headline>
    <Flex direction="horizontal">
      <div className="speed__Control">
        <Range pins={pins} callbackOnChange={onSpeedChange} value={speed} min={0} max={100} step={1}/>
      </div>
    </Flex>
  </div>
)};

const Audio = ({volume, speed, onSpeedChange, onVolumeChange}: AudioProps) => {
  return(
    <div className="audio">
      <Volume volume={volume} onVolumeChange={onVolumeChange} pins={["0%", "25%", "50%", "75%", "100%"]} />
      <Speed speed={speed} onSpeedChange={onSpeedChange} pins={["0.5x", "0.75x", "1.0x", "1.25x", "1.5x"]} />
    </div>
  )
}

export {
  Volume,
  Speed
}
export default Audio;