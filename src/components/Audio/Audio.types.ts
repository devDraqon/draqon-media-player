export interface VolumeProps {
  pins: Array<string>;
  onVolumeChange?: any;
  volume?: any;
}
export interface SpeedProps {
  pins: Array<string>;
  onSpeedChange?: any;
  speed?: any;
}
export interface AudioProps {
  onSpeedChange?: any;
  onVolumeChange?: any;
  volume?: any;
  speed?: any;
}