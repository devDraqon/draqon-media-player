import React from "react";
import Audio, {Speed, Volume} from './Audio';

export default {
  title: "Body Atoms|Audio"
};

export const SpeedRange = () => <Speed  pins={["0.5x", "0.75x", "1.0x", "1.25x", "1.5x"]} />;
export const VolumeRange = () => <Volume  pins={["0%", "25%", "50%", "75%", "100%"]} />;
export const AudioRanges = () => <Audio />
