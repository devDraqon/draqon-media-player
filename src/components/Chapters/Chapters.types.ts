export interface ChaptersProps {
  chapters: Array<{
    endtime: string;
    title: string;
  }>;
  changeChapterCallback?: any;
}