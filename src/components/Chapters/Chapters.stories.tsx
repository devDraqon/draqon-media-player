import React from "react";
import Chapters from './Chapters';

export default {
  title: "Body Atoms|Chapters"
};

export const Default = () => <Chapters chapters={[
  {
    title: "Prolog",
    endtime: "06:21"
  },
  {
    title: "Feedback",
    endtime: "08:49"
  },
  {
    title: "Religion Abschaffen?",
    endtime: "19:56"
  },
  {
    title: "Kapitalismus als Waffe",
    endtime: "09:11"
  },
  {
    title: "Politiker auf Abwegen",
    endtime: "12:42"
  },
  {
    title: "Die Grenze des Lobbyismus",
    endtime: "11:23"
  },
  {
    title: "Epilog",
    endtime: "06:26"
  },
  {
    title: "Bonustrack",
    endtime: "01:17"
  }
]} />;
