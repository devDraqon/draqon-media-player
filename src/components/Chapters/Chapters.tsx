import React from "react";
import { ChaptersProps } from "./Chapters.types";
import {Paragraph, Flex} from "draqon-component-library";
import "./Chapters.scss";

const Chapters: React.FC<ChaptersProps> = ({ chapters, changeChapterCallback }) => {
  
  const changeChapter = (id) => {
    changeChapterCallback ? changeChapterCallback(id) : null;
  }

  return (
    <div className="chapters">
      <Flex direction="vertical" classList={["chapters__list"]}>
        {chapters.map((chapter, id) =>
          <span onClick={() => changeChapter(id)}>
            <Flex  key={id} direction="horizontal" classList={["chapter"]}>
              <Paragraph classList={["chapter__id"]} size="small"> {(id+1)} </Paragraph>
              <Paragraph classList={["chapter__title"]} size="small"> {chapter.title} </Paragraph>
              <Paragraph classList={["chapter__time"]} size="small"> {chapter.endtime} </Paragraph>
            </Flex>
          </span>
        )}
        </Flex>
    </div>
)};

export default Chapters;